package com.example.countdown;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new CountDownTask().execute();
    }
    private class CountDownTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
            tvCounter.setText("*START*");
        }
        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 15; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i);
                } catch (InterruptedException e) {
                }
            }
            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
            tvCounter.setText(Integer.toString(values[0].intValue()));
        }
        @Override
        protected void onPostExecute(Void result) {
            TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
            tvCounter.setText("*DONE*");
        }
    }
}